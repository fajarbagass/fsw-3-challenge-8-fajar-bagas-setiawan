const ApplicationController = require("./ApplicationController");
const { NotFoundError } = require("../errors");

describe("ApplicationController", () => {
    describe("handleGetRoot", () => {
        it("should return a 200 status code", () => {
            const mockRequest = {};
            const mockResponse = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn().mockReturnThis(),
            };
            const applicationController = new ApplicationController();
            applicationController.handleGetRoot(mockRequest, mockResponse);
            expect(mockResponse.status).toHaveBeenCalledWith(200);
        });
    });
    describe("handleNotFound", () => {
        it("should return a 404 status code", () => {
            const mockRequest = {
                method: "GET",
                url: "/blabla",
            };
            const mockResponse = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn().mockReturnThis(),
            };
            const applicationController = new ApplicationController();
            const err = new NotFoundError(mockRequest.method, mockRequest.url);
            applicationController.handleNotFound(mockRequest, mockResponse);
            expect(mockResponse.status).toHaveBeenCalledWith(404);
            expect(mockResponse.json).toHaveBeenCalledWith({
            error: {
                name: err.name,
                message: err.message,
                details: err.details,
            },
            });
        });
    });
    describe("handleError", () => {
        it("should return a 500 status code", () => {
            const err = {};
            const mockRequest = {};
            const mockResponse = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn().mockReturnThis(),
            };
            const applicationController = new ApplicationController();
            applicationController.handleError(err, mockRequest, mockResponse);
            expect(mockResponse.status).toHaveBeenCalledWith(500);
            expect(mockResponse.json).toHaveBeenCalledWith({
                error: {
                    name: err.name,
                    message: err.message,
                    details: err.details || null,
                },
            });
        });
    });
    describe("getOffsetFromRequest", () => {
        it("should return a number", () => {
            const mockRequest = { 
            query: { 
                page: 1, 
                pageSize: 10 
            } 
            };
            const applicationController = new ApplicationController();
            const offset = applicationController.getOffsetFromRequest(mockRequest);
            expect(offset).toBe(0);
        });
    });
    describe("buildPaginationObject", () => {
        it("should return an object", () => {
            const mockRequest = { 
            query: { 
                page: 1, 
                pageSize: 10 
            } 
            };
            const count = 10;
            const applicationController = new ApplicationController();
            const paginationObject = applicationController.buildPaginationObject(mockRequest, count);
            expect(paginationObject).toEqual({
                page: 1,
                pageCount: 1,
                pageSize: 10,
                count: 10,
            });
        });
    });
});
